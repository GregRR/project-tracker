module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bowercopy: {
			options: {
				runBower: true
			},
			libs: {
					options: {
							destPrefix: 'public'
					},
					files: {
							'js/jquery.min.js': 'jquery/dist/jquery.min.js',
							'js/bootstrap.min.js': 'bootstrap-sass/assets/javascripts/bootstrap.min.js',
							'fonts/bootstrap': 'bootstrap-sass/assets/fonts/bootstrap/*'
					}
			}
    },
    copy: {
			main: {
				src: 'src/js/project-tracker.js',
				dest: 'public/js/project-tracker.js',
			},
			vendor: {
				src: 'src/js/vendor/bootstrap-datepicker.min.js',
				dest: 'public/js/bootstrap-datepicker.min.js',
			},
			img: {
				files: [
					{ src: 'src/img/gantt.png', dest: 'public/img/gantt.png' },
					{ src: 'src/img/pt-logo-2.png', dest: 'public/img/pt-logo.png' }
				]
			}
		},
		sass: {                             
			dist: {                           
				options: {                     
					style: 'compressed'
				},
				files: {                         
					'public/css/style.css': 'src/scss/style.scss',
				}
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass']
			},
			js: {
				files: '**/*.js',
				tasks: ['copy']
			}
		}
  });

  grunt.loadNpmTasks('grunt-bowercopy');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  
  grunt.registerTask('default', ['bowercopy', 'copy', 'sass', 'watch']);

};