// project model
var mongoose = require('mongoose');

// Define project schema
var ProjectSchema = new mongoose.Schema({
  name: { type: String },
  type: { type: String },
  description: { type: String },
  start_date: String, // D MMM YYYY, should eventually use Date type
  due_date: String,		// D MMM YYYY
  status: String,			// New, In Progress, In QA, Reviewing, Closed
  created_by: { type: String },
  parent: String
});

// Export Mongoose model
module.exports = mongoose.model('Project', ProjectSchema);
