// Required packages
var express = require('express');
var exphbs  = require('express-handlebars');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var multer  = require('multer');
var methodOverride = require('method-override');
//var passport = require('passport');
//var expressSession = require('express-session');

// Use controllers
var portalController = require('./controllers/portal');
var projectController = require('./controllers/project');
//var userController = require('./controllers/user');


mongoose.connect('mongodb://localhost:27017/projecttracker');


var app = express();

// set static folder for css, js, and whatnot
app.use('/public', express.static('public'));

// set folder/configure file uploads
app.use(multer({ dest: './uploads/',
 rename: function (fieldname, filename) {
    return filename+Date.now();
  },
	onFileUploadStart: function (file) {
		console.log(file.originalname + ' is starting ...')
	},
	onFileUploadComplete: function (file) {
		console.log(file.fieldname + ' uploaded to  ' + file.path)
		done=true;
	}
}));

// user Handlebars for views
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Use body-parser package
app.use(bodyParser.urlencoded({
  extended: true
}));

// allows accepting PUT and DELETE using hidden form fields
app.use(methodOverride('_method'));

// Use and config Passport, etc (user auth/session packages)
//app.use(expressSession({secret: 'mySecretKey'}));
//app.use(passport.initialize());
//app.use(passport.session());

// Use env defined port or 5000
var port = process.env.PORT || 5000;

// Create instance of Express router
var router = express.Router();


// PORTAL ROUTES ======================================================
router.route('/project-new')
  .get(portalController.getNewProjectForm);

router.route('/signin')
  .get(portalController.getSignInForm);
  
router.route('/register')
  .get(portalController.getRegisterForm);
  
router.route('/')
  .get(portalController.getHomePage);  

// == FILE ROUTES ==================================================
// need to split out controller
app.post('/api/files',function(req,res){
  if ( done == true ) {
    console.log(req.files);
    res.end("File uploaded.");
  }
});

// == PROJECT ROUTES ================================================
// Create endpoint handlers for /projects
router.route('/projects')
  .post(projectController.postProjects)
  .get(projectController.getProjects);

// Create endpoint handlers for /projects/:project_id
router.route('/projects/:project_id')
  .get(projectController.getProject)
  .put(projectController.putProject)
  .delete(projectController.deleteProject);

// Create endpoint handlers for /projects/:project_id/edit
router.route('/projects/:project_id/edit')
  .get(projectController.getProjectEditForm);
  
// Create endpoint handlers for /projects/:project_id/add
// for adding sub-projects
router.route('/projects/:project_id/add')
  .get(projectController.postSubProjectForm);

// == USER ROUTES =====================================================  
// Create endpoint handlers for /users
//router.route('/users')
//  .post(userController.postUsers)
//  .get(userController.getUsers);


// Apply routes too app
app.use('/', router);

// Start the server
app.listen(port);
console.log('Running app on http://localhost:' + port);
