#Project Tracker
A simple demo project tracking app built using:

Node.js, Express, MongoDB, Mongoose, Twitter Bootstrap, and Sass.

Note: assumes [Node.js](https://nodejs.org/download/) and [homebrew](http://brew.sh/) (MacOSX) are already installed.

###Install and setup [MongoDB](http://docs.mongodb.org/manual/)
```
  $ brew install mongodb --with-openssl // or however you want to install it
  $ mkdir -p /data/db    // make sure user has write permissions here
```
###Start MongoDB
```
  $ mongod
```

### Build
```
  $ npm install
  $ grunt
```

### Run app
```
  $ node app.js
```
In browser, go to: http://localhost:5000
