var Project = require('../models/project');

// Create endpoint /projects for POST
exports.postProjects = function(req, res) {
  var project = new Project();

  // Set the project properties that came from POST data
  project.name = req.body.name;
  project.type = req.body.type;
  project.description = req.body.description;
  project.start_date = req.body.start_date;
  project.due_date = req.body.due_date;
  project.status = req.body.status;
  project.created_by = req.body.created_by;
  project.parent = req.body.parent;

  // Save project and check for errors
  project.save(function(err) {
    if (err) {
      console.log(err);
    	return res.send(err);
		}
    //res.json({ message: 'Project added.', data: project });
    //back to projects page
    res.redirect('/projects');
  });
};


// Create endpoint /projects for GET (all projects)
exports.getProjects = function(req, res) {
  // Get all projects
  Project.find(function(err, projects) {
    if (err) {
      return res.send(err);
	 	}
		res.render('projects', { 
				projects: projects,
				pageTitle: 'Projects',
  			headerVersion: 'main' 
  	});
  });
};
  

// Get a single project, plus it's sub-projects
exports.getProject = function(req, res) {
	// get parent project
	var project_id = req.params.project_id;
	
	// Create an array to hold the sub projects
	var sub_projects = [];
	
	// Get the child projects
	Project.find({parent: project_id}, function(err, docs) {
			if (err) {
				return res.send(err);
			}
			sub_projects = docs;
	});
		 
	Project.findById( project_id, function(err, project) {	
		 if (err) {
      return res.send(err);
     }
		 res.render('project', { 
					project: project, 
					pageTitle: 'Project', // for the <title> tag
					headerVersion: 'main',
					sub_projects: sub_projects 
		 });
	});
};


// Create endpoint /projects/:project_id for PUT (UPDATE)
exports.putProject = function(req, res) {
  // Find specific Project
  var project_id = req.params.project_id;
  Project.findById(req.params.project_id, function(err, project) {
    if (err) {
      return res.send(err);
		}
    // Update existing project
    project.name = req.body.name;
		project.type = req.body.type;
		project.description = req.body.description;
		project.start_date = req.body.start_date;
		project.due_date = req.body.due_date;
		project.status = req.body.status;
		project.created_by = req.body.created_by;
		project.parent = req.body.parent;

    // Save project
    project.save(function(err) {
      if (err) {
        return res.send(err);
			}
      // res.json(project);
      //back to projects page
    	res.redirect('/projects/'+ project_id);
    });
  });
};


// Create endpoint /project/:project_id for DELETE
exports.deleteProject = function(req, res) {
  // Find project and remove
  Project.findByIdAndRemove(req.params.project_id, function(err) {
    if (err) {
      return res.send(err);
		}
    res.redirect('/projects');
  });
};


// Create endpoint /projects/:project_id/edit for GET (EDIT form)
exports.getProjectEditForm = function(req, res) {
  //Find the specific project
  Project.findById(req.params.project_id, function(err, project) {
    if (err) {
      return res.send(err);
		}
    res.render('project-edit', { 
    		project: project, 
				pageTitle: 'Project',
  			headerVersion: 'main',
  			id: project._id
  	});
  });
};

// Form for adding subproject that will include parent ID 
exports.postSubProjectForm = function(req, res) {
	var parentId = req.params.project_id; 
  res.render('project-new', {
  	parent: parentId,
  	pageTitle: 'New project',
  	headerVersion: 'main'
  });
};
