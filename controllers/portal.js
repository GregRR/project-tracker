//  portal.js

exports.getHomePage = function(req, res) {
  res.render('index', {
  	pageTitle: 'Home',
  	headerVersion: 'home'
  });
};

exports.getSignInForm = function(req, res) {
  res.render('signin', {
  	pageTitle: 'Sign in',
  	headerVersion: 'main'
  });
};

exports.getRegisterForm = function(req, res) {
  res.render('register', {
  	pageTitle: 'Register',
  	headerVersion: 'main'
  });
};

exports.getNewProjectForm = function(req, res) {
  res.render('project-new', {
  	pageTitle: 'New project',
  	headerVersion: 'main'
  });
};
