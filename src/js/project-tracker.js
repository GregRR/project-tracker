// main javascript

// cancel out of forms by returning to previous page
$( ".page-cancel" ).click(function() {
	window.history.go(-1); return false;
});

// Jquery form init
$(document).ready(function() {
	$("#upload-form").submit(function() {
		$(this).ajaxSubmit({
				error: function(xhr) {
								status("Error: " + xhr.status);
				},
				success: function(response) {
									console.log(response);
				}
		});
	// disable page refresh
	return false;
	});    
});

// table filter
$(document).ready(function() {
    $("#table-filter").keyup(function() {
        var tableBody = $(".filterable-table tbody");
        var tableRowsClass = $(".filterable-table tbody tr");
        $(".search-feedback").remove();
        tableRowsClass.each(function(i, val) {
            //Lowercase stuff for case insensitivity
            var rowText = $(val).text().toLowerCase();
            var inputText = $(this).val().toLowerCase();

            if ( rowText.indexOf( inputText ) == -1 ) {
                tableRowsClass.eq(i).hide(); 
            } else {
                $('.search-sf').remove();
                tableRowsClass.eq(i).show();
            }
        });
        // if results = 0, show msg
        if( tableRowsClass.children(':visible').length == 0 ) {
            tableBody.append('<tr class="search-feedback"><td class="text-muted" colspan="6">No entries found.</td></tr>');
        }
    });
});
